"""
=======================================================================================================================================================================================================================
This program is designed around the idea of a simple Caeser Cipher. Julius Caeser use to encrpt his
messages by changing each letter to be three letters down from it's current spot in the alphabet. 

This program follows the same idea, but instead of changing the characters based on their alphabetical ordering
it will change them based on their ascii value subtracting three from the characters current value encrypting
the message. 

This isn't designed to be a secure encryption obviously just a bit of fun to play around with. 
=======================================================================================================================================================================================================================
"""





from tkinter import * 
from tkinter import messagebox

Num = 3

def error_message(): 
    """
    An Error message to be displayed if something goes wrong. 
    """
    messagebox.showinfo("Alert","Something went wrong, please check the file type only text files such as .txt, .cpp, ect. are valid")
def encrypt(path,text):
    """
    Encrypts the contents of the file by subtracting three from it's ascii value. 
    """
    try:
        result = []
        secret_str = ""
        for letter in text: 
            for unit in letter:
                letter_ascii = ord(unit) - Num
                result.append(letter_ascii)
    except: 
        error_message()
        gui_page_one()
    messagebox.showinfo("Alert","Encryption Successful!")
    output_to_file(path,result, False)

def decrypt(path,result): 
    """
    Decrypts a file that was encrypted by this program by adding three to it's ascii value. 
    """
    try: 
        end_str = ""
        for numbers in result:     
            numbers = ord(numbers) + Num
            end_str = end_str + chr(numbers)
    except:
        error_message()
        gui_page_one()
    messagebox.showinfo("Alert","Decryption Successful!")
    output_to_file(path,end_str, True) 

def get_file_input(path,answer): 
    """
    Uses the path and answer provided by the user to find and Encrypt or Decrypt it's contents.
    """
    try:
        answer = int(answer)
        file_to_use = open(path, "r") 
        if file_to_use.mode == "r": 
            contents = file_to_use.read()
        if answer == 1: 
            encrypt(path,contents)
        else: 
            decrypt(path,contents)
        file_to_use.close()
    except: 
        error_message()
        gui_page_one()

def output_to_file(path,end_list,decrypt): 
    """
    Re-writes the file based on the path and if the user decided to encrypt or decrypt. 
    """
    try:
        output = ""
        file_to_use = open(path, "w")
        if decrypt == False: 
            for item in end_list: 
                item = chr(item)
                output = output + item 
        else: 
            for i in end_list: 
                output = output + i 
        file_to_use.write(output)
        file_to_use.close()
    except: 
        error_message()
        gui_page_one()
def gui_page_one(): 
    """
    The "Home Page" of the program. Asks the user if they want to encrypt or decrypt a file. 
    """
    root = Tk()
    start_message = Label(root, text="Would you like to encrypt or decrypt a file?\n")
    start_message.pack()
    Encrypt = Button(root, text="Encrypt", command=lambda:[root.destroy(),gui_page_two(1)])
    Encrypt.pack()
    Decrypt = Button(root, text="Decrypt",command=lambda:[root.destroy(),gui_page_two(2)])
    Decrypt.pack()
    root.mainloop()

def gui_page_two(answer): 
    """
    The second page of the program. Prompts the user to enter the path of the 
    file to be encrypted/decrypted. 
    """
    root = Tk()
    path_message = Label(root, text="Please enter the path to the text file below:")
    path_message.pack()
    path_entry = Entry(root)
    path_entry.pack() 
    if answer == 1:
        power_button = Button(root, text="Encrypt", command=lambda:[get_file_input(path_entry.get(), 1),root.destroy(),gui_page_one()])
    else:
        power_button = Button(root, text="Decrypt", command=lambda:[get_file_input(path_entry.get(), 2),root.destroy(),gui_page_one()])
    power_button.pack()
    root.mainloop()
def main(): 
    """
    Main function of the program that calls the main page of the gui. 
    """
    gui_page_one()
    

if __name__ == '__main__': 
    main()